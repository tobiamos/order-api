const crypto = require('crypto');

const primeLength = 100;
const diffHell = crypto.createDiffieHellman(primeLength);

diffHell.generateKeys('base64');
console.log('Public Key : ', diffHell.getPublicKey('base64'));
console.log('Private Key : ', diffHell.getPrivateKey('base64'));

console.log('Public Key : ', diffHell.getPublicKey('hex'));
console.log('Private Key : ', diffHell.getPrivateKey('hex'));
