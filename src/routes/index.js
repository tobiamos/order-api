const express = require('express');

const router = express.Router();
const userRoutes = require('../modules/User/routes');
const adminRoutes = require('../modules/Admin/routes');
const productRoutes = require('../modules/Products/routes');
const orderRoutes = require('../modules/Orders/routes');
const { sendJSONResponse } = require('../helpers');

router.get('/', (req, res) => sendJSONResponse(res, 200, null, req.method, 'Api is connected'));
router.use('/user', userRoutes);
router.use('/admin', adminRoutes);
router.use('/product', productRoutes);
router.use('/order', orderRoutes);


module.exports = router;
