module.exports = {
  env: 'production',
  db: process.env.DBURI,
  port: process.env.PORT || 4000,
  secret: process.env.SECRET,
  mail: {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
};
