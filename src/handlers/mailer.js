const nodeMailer = require('nodemailer');
const pug = require('pug');
const juice = require('juice');
const htmlToText = require('html-to-text');
const { mail } = require('../config');

const transport = nodeMailer.createTransport({
  host: mail.host,
  port: mail.port,
  auth: {
    user: mail.user,
    pass: mail.pass,
  },
});

const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(`${__dirname}/../views/email/${filename}.pug`, options);
  const inlined = juice(html);
  return inlined;
};

module.exports.sendEmail = async (options) => {
  const html = generateHTML(options.fileName, options);
  const text = htmlToText.fromString(html);
  const mailOptions = {
    from: mail.sender,
    to: options.admin.email,
    subject: options.subject,
    html,
    text,
  };
  return transport.sendMail(mailOptions);
};
