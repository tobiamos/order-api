const { verify } = require('jsonwebtoken');
const { promisify } = require('util');
const { sendJSONResponse } = require('../helpers');
const { secret } = require('../config');

const verifyPromise = promisify(verify);

module.exports.authenticate = async (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return sendJSONResponse(
      res,
      401,
      null,
      req.method,
      'Authentication Failed, Please provide an authentication token',
    );
  }
  try {
    const decoded = await verifyPromise(token, secret);
    req.decoded = decoded;
    return next();
  } catch (error) {
    return sendJSONResponse(res, 401, { error }, req.method, 'Authentication Failed');
  }
};

module.exports.authorize = (req, res, next) => {
  if (req.decoded.userType !== 'admin') {
    return sendJSONResponse(res, 401, null, req.method, 'This route is restricted to admin access only');
  }
  return next();
};
