const joi = require('joi');

module.exports.createProduct = {
  body: {
    name: joi.string().required(),
    amount: joi.number().required(),
  },
};

module.exports.updateProduct = {
  body: {
    name: joi.string().optional(),
    amount: joi.number().optional(),
    isStocked: joi.boolean().optional(),
  },
  params: {
    productId: joi.string().required(),
  },
};

module.exports.deleteProduct = {
  params: {
    productId: joi.string().required(),
  },
};

module.exports.getProducts = {
  query: {
    page: joi.number().optional(),
    limit: joi.number().max(100).optional(),
    q: joi.string().optional(),
  },
};
