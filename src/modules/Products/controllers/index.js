const mongoose = require('mongoose');
const { sendJSONResponse } = require('../../../helpers');

const Product = mongoose.model('Product');

module.exports.createProduct = async (req, res) => {
  const { name } = req.body;
  const existingProduct = await Product.findOne({ name });
  if (existingProduct) {
    return sendJSONResponse(res, 400, null, req.method, 'A product with that name already exists');
  }
  const product = await new Product(req.body).save();
  return sendJSONResponse(res, 200, product, req.method, 'Created new product');
};

module.exports.getAllProducts = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit || 20;
  const skip = (page * limit) - limit;
  const productPromise = Product
    .find()
    .skip(skip)
    .limit(limit)
    .sort({ createdAt: 'desc' });
  const countPromise = Product.find().count();
  const [products, count] = await Promise.all([productPromise, countPromise]);
  const pages = Math.ceil(count / limit);
  if (!products.length && skip) {
    return sendJSONResponse(res, 404, null, req.method, 'No products found');
  }

  return sendJSONResponse(res, 200, {
    products, page, pages, count,
  }, req.method, 'All products fetched');
};

module.exports.getOneProduct = async (req, res) => {
  const product = await Product.findById(req.params.productId);
  if (!product) {
    return sendJSONResponse(res, 404, null, req.method, 'Product not found');
  }
  return sendJSONResponse(res, 200, product, req.method, 'Product found');
};

module.exports.updateProduct = async (req, res) => {
  const { name, amount, isStocked } = req.body;
  const product = await Product.findById(req.params.productId);
  if (!product) {
    return sendJSONResponse(res, 404, null, req.method, 'Product not found');
  }
  name !== undefined ? product.name = name : product.name = product.name;
  amount !== undefined ? product.amount = amount : product.amount = product.amount;
  isStocked !== undefined ? product.isStocked = isStocked : product.isStocked = product.isStocked;
  await product.save();
  return sendJSONResponse(res, 200, product, req.method, 'Product updated succesfully');
};

module.exports.deleteProduct = async (req, res) => {
  await Product.findByIdAndRemove(req.params.productId);
  return sendJSONResponse(res, 200, null, req.method, 'Deleted product');
};
