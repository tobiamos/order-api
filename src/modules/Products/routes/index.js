const express = require('express');
const productController = require('../controllers');
const productPolicies = require('../policies');
const { catchErrors, validate } = require('../../../helpers');
const { authenticate, authorize } = require('../../../middlewares');

const router = express.Router();

router.post(
  '/',
  catchErrors(authenticate),
  authorize,
  validate(productPolicies.createProduct),
  catchErrors(productController.createProduct),
);

router.get(
  '/:productId',
  catchErrors(authenticate),
  authorize,
  catchErrors(productController.getOneProduct),
);

router.get(
  '/',
  catchErrors(authenticate),
  validate(productPolicies.getProducts),
  catchErrors(productController.getAllProducts),
);

router.put(
  '/:productId',
  catchErrors(authenticate),
  authorize,
  validate(productPolicies.updateProduct),
  catchErrors(productController.updateProduct),
);

router.delete(
  '/:productId',
  catchErrors(authenticate),
  authorize,
  validate(productPolicies.deleteProduct),
  catchErrors(productController.deleteProduct),
);

module.exports = router;
