const express = require('express');
const orderController = require('../controllers');
const orderPolicies = require('../policies');
const { catchErrors, validate } = require('../../../helpers');
const { authenticate } = require('../../../middlewares');


const router = express.Router();

router.post(
  '/',
  catchErrors(authenticate),
  validate(orderPolicies.createOrder),
  catchErrors(orderController.createOrder),
);

router.get(
  '/',
  catchErrors(authenticate),
  validate(orderPolicies.getAllOrders),
  catchErrors(orderController.getAllOrders),
);

router.get(
  '/:orderId',
  catchErrors(authenticate),
  validate(orderPolicies.getOneOrder),
  catchErrors(orderController.getOneOrder),
);


module.exports = router;
