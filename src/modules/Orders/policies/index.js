const joi = require('joi');


module.exports.createOrder = {
  body: {
    total: joi.number().required(),
    items: joi.array()
      .items(joi
        .object()
        .keys({
          name: joi.string().required(),
          quantity: joi.number().min(0).required(),
          amount: joi.number().min(0).required(),
        })).min(1).required(),
  },
};

module.exports.getAllOrders = {
  query: {
    page: joi.number().optional(),
    limit: joi.number().max(100).optional(),
  },

};

module.exports.getOneOrder = {
  body: {
    params: {
      orderId: joi.string().required(),
    },
  },
};
