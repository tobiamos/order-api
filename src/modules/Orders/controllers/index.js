const mongoose = require('mongoose');
const { sendJSONResponse } = require('../../../helpers');
const hashId = require('../../../handlers/hashid');
const { sendEmail } = require('../../../handlers/mailer');

const Order = mongoose.model('Order');
const User = mongoose.model('User');

module.exports.createOrder = async (req, res) => {
  const { items, total } = req.body;
  const order = new Order();
  order.items = items;
  order.invoice = hashId.generateUnique();
  order.total = total;
  order.orderedBy = req.decoded._id;
  const user = await User.findById(req.decoded._id);
  const options = {
    admin: user,
    fileName: 'new-order',
    subject: `${order.invoice} - New Order`,
    items,
  };
  sendEmail(options);
  await order.save();
  return sendJSONResponse(res, 200, order, req.method, 'Order Created');
};

module.exports.getAllOrders = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit || 20;
  const skip = (page * limit) - limit;
  const orderPromise = Order
    .find({ orderedBy: req.decoded._id })
    .skip(skip)
    .limit(limit)
    .sort({ createdAt: 'desc' });
  const countPromise = Order.find().count();
  const [orders, count] = await Promise.all([orderPromise, countPromise]);
  const pages = Math.ceil(count / limit);
  if (!orders.length && skip) {
    return sendJSONResponse(res, 404, null, req.method, 'No orders found');
  }

  return sendJSONResponse(res, 200, {
    orders, page, pages, count,
  }, req.method, 'All orders fetched');
};

module.exports.getOneOrder = async (req, res) => {
  const { orderId } = req.params;
  const order = await Order.findOne({ _id: orderId, orderedBy: req.decoded._id });
  if (!order) {
    return sendJSONResponse(res, 404, null, req.method, 'Orders not found');
  }
  return sendJSONResponse(res, 200, order, req.method, 'Order Found');
};
