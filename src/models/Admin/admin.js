const mongoose = require('mongoose');
const { randomBytes, pbkdf2Sync } = require('crypto');
const { sign } = require('jsonwebtoken');
const { secret } = require('../../config');

const adminSchema = new mongoose.Schema({
  name: {
    type: String, required: 'Please provide a name', lowercase: true, trim: true,
  },
  email: {
    type: String, required: 'Please provide an email', lowercase: true, trim: true,
  },
  hash: String,
  salt: String,
  userType: { type: String, default: 'admin' },
}, { timestamps: true });

adminSchema.methods.setPassword = function adminPassword(password) {
  this.salt = randomBytes(16).toString('hex');
  this.hash = pbkdf2Sync(password, this.salt, 100, 64, 'sha512').toString('hex');
};

adminSchema.methods.verifyPassword = function verify(password) {
  const hash = pbkdf2Sync(password, this.salt, 100, 64, 'sha512').toString('hex');
  return this.hash === hash;
};

adminSchema.methods.generateJWT = function token() {
  return sign(
    {
      _id: this._id,
      name: this.name,
      email: this.email,
      userType: this.userType,
    },
    secret, {
      issuer: 'https://fieldinsight.co',
      expiresIn: '7d',
    },
  );
};

module.exports = mongoose.model('Admin', adminSchema);
