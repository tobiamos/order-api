const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: String,
  amount: Number,
  isStocked: { type: Boolean, default: true },
}, { timestamps: true });

module.exports = mongoose.model('Product', productSchema);
