const mongoose = require('mongoose');
const { status } = require('../../utils');

const itemSchema = new mongoose.Schema({
  name: String,
  quantity: String,
  amount: Number,
});

const orderSchema = new mongoose.Schema({
  items: [itemSchema],
  total: Number,
  invoice: String,
  status: {
    type: String, default: status.open, enum: Object.values(status), required: true, lowercase: true,
  },
  orderedBy: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
  },

}, { timestamps: true });

module.exports = mongoose.model('Order', orderSchema);
